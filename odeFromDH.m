function [DH, H, D, C, G, y0, dy0, q, dq, u] = odeFromDH(table)

    %Read DH table
    m = readmatrix(table);

    %Count number of frames
    links = size(m, 1);

    %State variables and state variable velocities
    syms q [links 1] real
    syms dq [links 1] real

    %Extract DH Table
    DH = sym(m(:, 2:6));
    y0 = extractVariablesFromDH(DH);
    dy0 = zeros(size(y0));
    u = m(:, 17);

    %Extract mass column
    mass = m(:, 7);
    
    %Extract center of mass vectors
    Lci = m(:, 11:13)';
    
    %Extract gravity direction vector
    g = m(1, 18:20)';
    
    %Extract Inertia info and create tensors
    I = zeros([3 3 links]);
    
    for i = 1:links
        I(:,:,i) = diag(m(i,14:16));
        
        % Set non-dynamic frame joint variables to NaN
        if DH(i, 1) < 0
            q(i) = NaN;
            dq(i) = NaN;
        end
    end

    DH = applyJointVartoDH(DH, q);
    [D, ~, H] = dfromDHTable(DH, q, Lci, mass, I, 0);

    H = simplify(H);
    D = simplify(D);
    C = simplify(getSymbolicC(D, q, dq));
    G = simplify(getSymbolicG(H, q, mass, Lci, g));
    
end
