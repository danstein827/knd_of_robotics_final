function [D, Jac, H] = dfromDHTable(DH, q, lc, m, I, isDeg)
    % Returns inertia matrix (D), Jacobian matrix (Jac), homogenous
    %   transforms(H).
    % Provide DH Table (DH), joint variables (q), center of mass vectors
    %   (lc), mass vector (m), and inertia tensor (I)

    links = size(DH, 1);
    DH = applyJointVartoDH(DH, q);
    H = homogeneousTransformFromDHTable(DH, 'onlyFinal', 0, 'isDeg', isDeg);
    
    if (isa(DH, 'sym') || isa(q, 'sym'))
        Jac = sym(zeros([6 links links]));
    else
        Jac = zeros([6 links links]);
    end
    
    for i = 1:size(DH, 1)
        Jac(:,:,i) = jacobianFromDHTable(DH(1:i, :), 'isDeg', isDeg, ...
            'nlinks', links, 'centerofmass', lc(:, i));
    end
    
    D = getInertiaMatrix(Jac, H, m, I);
end