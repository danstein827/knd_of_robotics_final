function playODE(DH, q, y)
figure();
ax = axes();

links = size(DH, 1);

H = homogeneousTransformFromDHTable(DH, 'onlyFinal', 0, 'isDeg', 0);

plot3(0,0,0, '+')
hold on
pbaspect([1 1 1])
grid on
grid minor
xlim([-5 5]); ylim([-5 5]); zlim([-5 5])
xticks(-5:5); yticks(-5:5); zticks(-5:5)
xlabel('x'); ylabel('y'); zlabel('z')
%view(0, 90)

h = subs(H, q, y(1, :)');

p = [0 0 0];
for c = 1:links
    p(c+1, :) = h(1:3, 4, c)';
end

plot3(p(:,1), p(:,2), p(:,3), 'b-o')
pause

ee = [];

disp("Running")
for i = 1:length(y)
    if mod(i, 50) == 1
        cla;
        h = subs(H, q, y(i, :)');

        p = [0 0 0];
        for c = 1:links
            p(c+1, :) = h(1:3, 4, c)';
        end
        ee(end+1,:) = p(end, :);
        plot3(p(:,1), p(:,2), p(:,3), 'b-o')
        plot3(ee(:,1), ee(:,2), ee(:,3), 'r-')
        pause(0.1)
    end
    
end

hold off