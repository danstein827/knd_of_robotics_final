function D = getInertiaMatrix(J, H, m, I)
    % Finds the Inertia D(q) matrix
    % J = Jacobian for each link, stacked along axis 3
    % H = Homogeneous Transfor of each link, stacked along axis 3
    % m = link mass vector
    % I = Inertia Tensor for each link, stacked along axis 3

    links = size(J, 3);
    
    if isa(J, 'sym') || isa(m, 'sym')
        D = sym(zeros([links links]));
    else
        D = zeros([links links]);
    end
    
    for i = 1:links
        D = D + m(i)*J(1:3,:,i)'*J(1:3,:,i) + ...                       % Linear velocity terms
            J(4:6,:,i)'*H(1:3,1:3,i)*I(:,:,i)*H(1:3,1:3,i)'*J(4:6,:,i); % Rotational Velocity terms
    end
end