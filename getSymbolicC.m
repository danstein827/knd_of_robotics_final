function C = getSymbolicC(D, q, dq)
    % Finds the C matrix (from christoffel symbols) for provided Inertia
    % matrix (D) with joint variables (q) and joint velocities (dq)
    % Used only for symbolic matricies
    
    links = size(D, 1);
    
    dD = sym(zeros([links links links]));    
    christoffel = dD;
    C = sym(zeros([links links]));
    
    for i = 1:links
        if not((isnan(q(i))) || q(i) == 0)
            dD(:,:,i) = diff(D, q(i));
        end
    end
    
    for i = 1:links
        for j = 1:links
            for k = 1:links
                christoffel(i,j,k) = 0.5*(dD(k,j,i) + dD(k,i,j) - dD(i,j,k));
            end
        end
    end
    
    for i = 1:links
        for j = 1:links
            for k = 1:links
                if not((isnan(dq(i))) || dq(i) == 0)
                    C(k,j) = C(k,j) + christoffel(i,j,k)*dq(i);
                end
            end
        end
    end
    
    
end