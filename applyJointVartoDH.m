function DH = applyJointVartoDH(DH, q)
    % Returns DH table after applying joint variables (q) to the respective
    % joints

    links = size(DH, 1);

    if size(q, 1) ~= links
        error("q length must equal DH rows")
    end
    
    for i = 1:links
        in = DH(i, 1)+2;
        if (in > 1) && (in <= 5)
            DH(i, in) = q(i);
        end
    end
end