function H = homogeneousTransformFromDHTable(DH, varargin)
    % Returns the homogeneous transforms from a passed DH Table
    % DH containts the DH table, each row corresponds to a link.
    %   Order of columns: link variable (0 = rev about z, 1 = pris along z,
    %       2 = pris along x, 3 = rev about x), theta, d, a, alpha.
    %   Link variable can equal -1 for an intermediate link.  q matrix
    %       should have a placeholder variable for that ignored link.
    % jointVar contains current joint variables (DH table contains initial values)
    % onlyFinal flag.  If true returns the H matrix of the end effector.
    %   If false returns a 4x4xn matrix, where each link H matrix is
    %   stacked along dim 3.
    
    links = size(DH, 1);        %Get number of links
    
    q = NaN;
    
    validJointVar = @(x) ((size(size(x), 2) == 2) && (max(size(x)) == links) ...
                    && (min(size(x)) == 1) && (isnumeric(x))) || isnan(x);
    validFlag = @(x) (x == 0) || (x == 1);
    
    p = inputParser;
    addRequired(p, 'DH');
    addOptional(p, 'jointVar', q, validJointVar);
    addParameter(p, 'onlyFinal', 1);
    addParameter(p, 'isDeg', 1, validFlag);
    parse(p, DH, varargin{:});
    
    q = p.Results.jointVar;
    onlyFinal = p.Results.onlyFinal;
    flagIsDeg = p.Results.isDeg;
    
    if onlyFinal    %Want only the final frame H or all previous frames
        if isa(DH, 'sym') || isa(q, 'sym')
            H = sym(eye(4));
        else
            H = eye(4);
        end
    else
        if isa(DH, 'sym') || isa(q, 'sym')
            H = sym(zeros([4 4 links]));
        else
            H = zeros([4 4 links]);
        end
    end
    
    for i = 1:links
        line = DH(i, 2:5);      %Get the line of the DH Matrix
        if ~isnan(q)
            try
                line(DH(i,1)+1) = q(i); %Insert the joint variable into the DH line
            catch

            end
        end
        
        %Calculate Homogeneous Tranformation Matricies
        if onlyFinal
            H = H*DH_Matrix(line(1), line(2), line(3), line(4), flagIsDeg);
        else
            if i == 1
                H(:,:,1) = DH_Matrix(line(1), line(2), line(3), line(4), flagIsDeg);
            else
                H(:,:,i) = H(:,:,i-1)*DH_Matrix(line(1), line(2), line(3), line(4), flagIsDeg);
            end
        end
    end
end