function dy = symbolicODE(t, y, D, C, G, Q, u)
    links = size(D, 2);
    
    if nargin == 6
        u = zeros([links 1]);
    end
    
    
    dq = y(2:2:2*links);
    
    dy = zeros([2*links 1]);
    
%     for i = 1:2*links
%         if mod(i, 2) == 1
%             % state variables stored in odd indicies
%             q((i+1)/2) = y(i);
%         else
%             % state variable velocities stored in even indicies
%             dq(i/2) = y(i);
%         end
%     end
    
    C = subs(C, Q, y);
    D = subs(D, Q, y);
    G = subs(G, Q, y);
    
    
    ddq = D\(u - C*dq - G);
    
    for i = 1:2*links
        if mod(i, 2) == 1
            dy(i) = dq((i+1)/2);
        else
            dy(i) = ddq(i/2);
        end
    end
end