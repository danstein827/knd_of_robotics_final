function jac = jacobianFromDHTable(DH, varargin)
    % Reads DH table and current joint variables (q) to return the velocity
    %   jacobian.
    % DH containts the DH table, each row corresponds to a link.
    %   Order of columns: link variable (0 = rev about z, 1 = pris along z,
    %       2 = pris along x, 3 = rev about x), theta, d, a, alpha
    % q contains current joint variables (DH table contains initial values)
    % nlinks - controls how many columns the jacobian should be
    % Center of mass - Vector to the center of mass.  !!!Must be provided in
    %   terms of the final frame!!!
    
    links = size(DH, 1);        %Get number of links
    
    validJointVar = @(x) (size(size(x), 2) == 2) && (max(size(x)) == links) ...
                    && (min(size(x)) == 1) && (isnumeric(x));
    validN = @(x) isscalar(x) && abs(x) == x;
    validFlag = @(x) (x == 0) || (x == 1);
    validVector = @(x) (isnumeric(x) || isa(x, 'sym')) && ...
                    (all(size(x) == [3 1]) || (all(size(x) == [1 3])));
    
    p = inputParser;
    addRequired(p, 'DH');
    addOptional(p, 'jointVar', NaN, validJointVar);
    addParameter(p, 'nlinks', links, validN);
    addParameter(p, 'isDeg', 1, validFlag);
    addParameter(p, 'centerofmass', [0;0;0], validVector);
    parse(p, DH, varargin{:});
    
    q = p.Results.jointVar;
    com = p.Results.centerofmass;
    
    %Preallocate array for Jacobian
    if isa(DH, 'sym') || isa(q, 'sym')
        jac = sym(zeros([6 p.Results.nlinks]));
    else
        jac = zeros([6 p.Results.nlinks]);     
    end
    
    H = homogeneousTransformFromDHTable(DH, q, 'onlyFinal', 0, ...
        'isDeg', p.Results.isDeg);
    
    %calculate jacobian
    for i = 1:links
        if DH(i,1) == 1 || DH(i,1) == 3
            %If prismatic joint
            if DH(i,1) == 1
                if i == 1   % Along zi axis
                    jac(1:3,1) = [0; 0; 1];
                else
                    jac(1:3,i) = H(1:3,3,i);
                end
            else            % Along xi axis
                if i == 1
                    jac(1:3,1) = [1; 0; 0];
                else
                    jac(1:3,i) = H(1:3,1,i);
                end
            end
        elseif DH(i,1) == 0 || DH(i,1) == 2
            %If revolute joint
            if i == 1       % Get origin vector to the joints
                O = H(1:3,:,end)*[com; 1];
                z = [0; 0; 1];
                x = [1; 0; 0];
            else
                O = H(1:3,:,end)*[com; 1] - H(1:3,4,i-1);
                z = H(1:3,3,i-1);
                x = H(1:3,1,i-1);
            end
            if DH(i,1) == 0     % about zi
                jac(1:3, i) = cross(z, O);
                jac(4:6, i) = z;
            else                % about xi
                jac(1:3, i) = cross(x, O);
                jac(4:5, i) = x;
            end
        else
            %If an intermediate frame with no joints to command
            jac(1:6, i) = zeros([6 1]);
        end
    end
end