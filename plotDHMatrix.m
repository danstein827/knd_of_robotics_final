function ax = plotDHMatrix(DH, ax, isDeg)
    % Generates a plot showing a robot configuration of given DH Table
    % DH containts the DH table, each row corresponds to a link.
    %   Order of columns: link variable (0 = rev about z, 1 = pris along z,
    %       2 = pris along x, 3 = rev about x), theta, d, a, alpha
    % Use applyJointVartoDH to change robot configuration

    % Generate homogeneous transformation for each link
    H = homogeneousTransformFromDHTable(DH, 'onlyFinal', 0, 'isDeg', isDeg);
    o = [0; 0; 0];
    % Plot the transforms    
    for i = 1:size(DH, 1)
        o = plotHomogeneousTransform(ax, H(:,:,i), o, 1);
    end
    
end