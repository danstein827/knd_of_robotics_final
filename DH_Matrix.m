function dh = DH_Matrix(theta, d, a, alpha, isDeg)
    if nargin == 4
        isDeg = 1;
    end
    
    R_theta = [RotationMatrix(theta, 'z', isDeg) [0; 0; 0]; 0 0 0 1];
    T_d = [eye(3) [0; 0; d]; 0 0 0 1];
    T_a = [eye(3) [a; 0; 0]; 0 0 0 1];
    R_alpha = [RotationMatrix(alpha, 'x', isDeg) [0; 0; 0]; 0 0 0 1];
    
    dh = R_theta*T_d*T_a*R_alpha;
end