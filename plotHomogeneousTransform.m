function [origin,out] = plotHomogeneousTransform(ax, H, Or, scale)
    % Plots homogeneous transformation on given axes

    if nargin == 2
        Or = [0;0;0];
        scale = 1;
    end
    
    if nargin == 3
        scale = 1;
    end
    
    o = [0; 0; 0; 1];
    x = [scale; 0; 0; 1];
    y = [0; scale; 0; 1];
    z = [0; 0; scale; 1];
    
    O = H*o;
    X = H*x-O;
    Y = H*y-O;
    Z = H*z-O;
    
    quiver3(Or(1),Or(2),Or(3), O(1)-Or(1), O(2)-Or(2), O(3)-Or(3), 1, ...
        'LineStyle', ':', 'Color', 'k', 'Parent',ax)
    quiver3(O(1), O(2), O(3), X(1), X(2), X(3), 1, 'Color', 'b', 'Parent',ax)
    quiver3(O(1), O(2), O(3), Y(1), Y(2), Y(3), 1, 'Color', 'r', 'Parent',ax)
    quiver3(O(1), O(2), O(3), Z(1), Z(2), Z(3), 1, 'Color', 'g', 'Parent',ax)
    
    origin = O;
    out = [X Y Z];
end